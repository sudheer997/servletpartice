


import com.servletpartice.jdc.EmployeeManagement;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/InsertRecord")
public class InsertRecord extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		long phoneNumber = Long.parseLong(request.getParameter("phoneNumber"));
		String gender = request.getParameter("gender");
		String emailId = request.getParameter("emailId");
		int skillId = Integer.parseInt(request.getParameter("skillId"));
		int projectId = Integer.parseInt(request.getParameter("projectId"));
		try {
			EmployeeManagement.insertRecord(firstName, lastName, emailId, phoneNumber, gender, skillId, projectId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		response.setContentType("text/plain; charset=ISO-8859-2");
		response.getWriter().println("insert record successfully");

	}

}
