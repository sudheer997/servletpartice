package com.servletpartice.jdc;

import java.sql.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class EmployeeManagement {

    public static Connection getConncetion() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        return DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/employees", "root", "welcome@ta");
    }

    public void showSkills() throws SQLException {
        Connection con = null;
        try {
            con = getConncetion();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from skills");
            while (rs.next())
                System.out.println(rs.getInt(1) + "  " + rs.getString(2));
        } catch (Exception e) {
            System.out.println(e);
        }
        finally {
            if (con != null)
                con.close();
        }
    }

    public void showProjects() throws SQLException {
        Connection con = null;
        try {
            con = getConncetion();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from projectdetails");
            while (rs.next())
                System.out.println(rs.getInt(1) + "  " + rs.getString(2));
        } catch (Exception e) {
            System.out.println(e);
        }
        finally {
            if (con != null)
                con.close();
        }
    }

    public static void insertRecord(String firstName, String lastName, String emailId, long phoneNumber,
                             String gender, int skillId, int projectId) throws SQLException {
        Connection con = null;
        try {
            con = getConncetion();
            Date date = new Date();
            long dateTime = date.getTime();
            String createdBy = "Admin";
            int employee_id = insertEmployeeRecord(con, emailId, skillId, dateTime, createdBy);
            if(employee_id != 1){
                insertPersonalRecord(con, employee_id, firstName, lastName, phoneNumber, gender, dateTime, createdBy);
                insertProjectrecord(con, employee_id, projectId, dateTime, createdBy);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        finally {
            if (con != null)
                con.close();
        }
    }

    public static int insertEmployeeRecord(Connection con,  String emailId, int skillId, long dateTime, String createBy){
        try {
            PreparedStatement employeeDetails = con.prepareStatement(
                    "insert into employeedetails(email_id, skills, create_by, create_time) values (?, ?, ?, ?)",
                    Statement.RETURN_GENERATED_KEYS);
            employeeDetails.setString(1, emailId);
            employeeDetails.setInt(2, skillId);
            employeeDetails.setString(3,createBy);
            employeeDetails.setLong(4,dateTime);
            employeeDetails.getGeneratedKeys();
            int noOfRecords = employeeDetails.executeUpdate();
            System.out.println(noOfRecords + " records inserted into employee details table");
            ResultSet rs = employeeDetails.getGeneratedKeys();
            if (rs.next()) {
                return rs.getInt(1);}
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return -1;
    }

    public static void insertPersonalRecord(Connection con, int employee_id, String firstName, String lastName,
                                     long phoneNumber, String gender, long dateTime, String createBy){
        try {
            PreparedStatement personalDetails = con.prepareStatement(
                    "insert into personaldetails(id, first_name, last_name, phonenumber, gender, " +
                            "create_time, create_by) values (?, ?, ?, ?, ?, ?, ?)");
            personalDetails.setInt(1, employee_id);
            personalDetails.setString(2, firstName);
            personalDetails.setString(3, lastName);
            personalDetails.setLong(4, phoneNumber);
            personalDetails.setString(5, gender);
            personalDetails.setLong(6, dateTime);
            personalDetails.setString(7, createBy);
            int noOfRecords = personalDetails.executeUpdate();
            System.out.println(noOfRecords + " records inserted into personal details table");

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void insertProjectrecord(Connection con, int employee_id, int projectId, long dateTime, String createBy){
        try {
            PreparedStatement projectDetails = con.prepareStatement(
                    "insert into employeeprojectdetails(employee_id, project_id, create_time, create_by) " +
                            "values (?, ?, ?, ?)");
            projectDetails.setInt(1, employee_id);
            projectDetails.setInt(2, projectId);
            projectDetails.setLong(3,dateTime);
            projectDetails.setString(4,createBy);
            int noOfRecords = projectDetails.executeUpdate();
            System.out.println(noOfRecords + " records inserted into project details table");
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public static Map<Integer, Employee> viewRecord() throws SQLException {
        Map<Integer, Integer> employeeIdMap = new HashMap<>();
        Map<Integer, Employee> employeeMap = new HashMap<>();
        Connection con = null;
        try {
            con = getConncetion();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT \n" +
                    "\temployeedetails.employee_id,\n" +
                    "    first_name,\n" +
                    "    last_name,\n" +
                    "    email_id,\n" +
                    "    phonenumber,\n" +
                    "    gender,\n" +
                    "    Name AS domain,\n" +
                    "    project_name\n" +
                    "FROM\n" +
                    "    (SELECT \n" +
                    "        employee_id, project_name\n" +
                    "    FROM\n" +
                    "        employeeprojectdetails\n" +
                    "    JOIN projectdetails ON employeeprojectdetails.project_id = projectdetails.project_id\n" +
                    "    WHERE\n" +
                    "        employeeprojectdetails.is_deleted = false) AS project\n" +
                    "        JOIN\n" +
                    "    employeedetails ON employeedetails.employee_id = project.employee_id\n" +
                    "        JOIN\n" +
                    "    personaldetails ON employeedetails.employee_id = personaldetails.id\n" +
                    "        JOIN\n" +
                    "    skills ON employeedetails.skills = skills.Id\n" +
                    "WHERE\n" +
                    "    employeedetails.is_deleted = false;");
            int serialNo = 0;
            while (rs.next()){
                employeeIdMap.put(++serialNo, rs.getInt(1));
                Employee employee = new Employee(rs.getInt(1));
                employee.setFirstName(rs.getString(2));
                employee.setLastName(rs.getString(3));
                employee.setEmailId(rs.getString(4));
                employee.setPhoneNumber(rs.getLong(5));
                employee.setGender(rs.getString(6));
                employee.setDomain(rs.getString(7));
                employee.setProjectName(rs.getString(8));
                employeeMap.put(serialNo, employee);

                /*System.out.println(serialNo + ". First name : " + rs.getString(2) + " Last name : " + rs.getString(3)
                        +" Email Id : " + rs.getString(4) + " Phone Number : " + rs.getLong(5) +
                        " Gender : " + rs.getString(6) + " Domain : " + rs.getString(7) + " project name : "+ rs.getString(8));*/
            }

        } catch (Exception e) {
            System.out.println(e);
        }
        finally {
            if (con != null)
                con.close();
        }
        return employeeMap;
    }

    public static void updatePersonalDetails(int employeeId, String fieldValue, String fieldName) throws SQLException {
        String sql = "update personaldetails set "+ fieldName + " = ?, modified_by = ?, modified_time = ? where id = ?";
        Connection con = null;
        Date date = new Date();
        long dateTime = date.getTime();
        try {
            con = getConncetion();
            PreparedStatement statement = con.prepareStatement(sql);
            statement.setString(1,fieldValue);
            statement.setString(2,"Admin");
            statement.setLong(3,dateTime);
            statement.setInt(4,employeeId);
            statement.executeUpdate();
            System.out.println("Updated record successfully");
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        finally {
            if (con != null)
                con.close();
        }
    }

    public static void updateDomain(int employeeId, int domainId) throws SQLException {
        String sql = "update employeedetails set skills = ?, modified_by = ?, modified_time = ?  where employee_id = ?";
        Connection con = null;
        Date date = new Date();
        long dateTime = date.getTime();
        try {
            con = getConncetion();
            PreparedStatement statement = con.prepareStatement(sql);
            statement.setInt(1, domainId);
            statement.setString(2,"Admin");
            statement.setLong(3,dateTime);
            statement.setInt(4,employeeId);
            statement.executeUpdate();
            System.out.println("Updated record successfully");
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        finally {
            if (con != null)
                con.close();
        }
    }

    public static void updateEmailId(int employeeId, String emailId) throws SQLException {
        String sql = "update employeedetails set email_id = ?, modified_by = ?, modified_time = ? where employee_id = ?";
        Connection con = null;
        Date date = new Date();
        long dateTime = date.getTime();
        try {
            con = getConncetion();
            PreparedStatement statement = con.prepareStatement(sql);
            statement.setString(1, emailId);
            statement.setString(2,"Admin");
            statement.setLong(3,dateTime);
            statement.setInt(4,employeeId);
            statement.executeUpdate();
            System.out.println("Updated record successfully");
        } catch (Exception e) {
            System.out.println(e);
        }
        finally {
            if (con != null)
                con.close();
        }
    }

    public void updateProjectDetails(int employeeId, int projectId) throws SQLException {
        String sql = "update employeeprojectdetails set project_id = ?, modified_by = ?, modified_time = ? where employee_id = ?";
        Connection con = null;
        Date date = new Date();
        long dateTime = date.getTime();
        try {
            con = getConncetion();
            PreparedStatement statement = con.prepareStatement(sql);
            statement.setInt(1, projectId);
            statement.setString(2,"Admin");
            statement.setLong(3, dateTime);
            statement.setInt(4, employeeId);
            statement.executeUpdate();
            System.out.println("Updated record successfully");
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        finally {
            if (con != null)
                con.close();
        }
    }

    public static void deleteRecord(int employeeId) throws SQLException {
        Connection con = null;
        try {
            con = getConncetion();
            deletePersonalRecord(con, employeeId);
            deleteEmployeeProjectRecord(con, employeeId);
            deleteEmployeeRecord(con, employeeId);
            System.out.println("deleted record successfully");
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        finally {
            if (con != null)
                con.close();
        }
    }

    public static void deleteEmployeeProjectRecord(Connection con, int employeeId){
        String sql = "update employeeprojectdetails set is_deleted = true where employee_id = ?";
        try {
            PreparedStatement statement = con.prepareStatement(sql);
            statement.setInt(1, employeeId);
            statement.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static void deletePersonalRecord(Connection con, int employeeId){
        String sql = "update personaldetails set is_deleted = true where id =?";
        try {
            PreparedStatement statement = con.prepareStatement(sql);
            statement.setInt(1, employeeId);
            statement.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static void deleteEmployeeRecord(Connection con, int employeeId){
        String sql = "update employeedetails set is_deleted = true where employee_id = ?";
        try {
            PreparedStatement statement = con.prepareStatement(sql);
            statement.setInt(1, employeeId);
            statement.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}

