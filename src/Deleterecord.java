

import com.servletpartice.jdc.EmployeeManagement;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/Deleterecord")
public class Deleterecord extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int employeeId = Integer.parseInt(request.getParameter("employeeId"));
		try {
			EmployeeManagement.deleteRecord(employeeId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		response.setContentType("text/plain; charset=ISO-8859-2");
		response.getWriter().println("delete record successfully");
	}

}
