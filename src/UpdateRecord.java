

import com.servletpartice.jdc.Employee;
import com.servletpartice.jdc.EmployeeManagement;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/UpdateRecord")
public class UpdateRecord extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int emploeeId = Integer.parseInt(request.getParameter("employeeId"));
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		int domainId = Integer.parseInt(request.getParameter("domain"));
		String emailID = request.getParameter("email");
		try {
			EmployeeManagement.updatePersonalDetails(emploeeId, firstName, "first_name");
			EmployeeManagement.updatePersonalDetails(emploeeId, lastName, "last_name");
			EmployeeManagement.updateEmailId(emploeeId, emailID);
			EmployeeManagement.updateDomain(emploeeId, domainId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		response.setContentType("text/plain; charset=ISO-8859-2");
		response.getWriter().println("update record successfully");
	}

}
