

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.servletpartice.jdc.EmployeeManagement;
import org.json.JSONObject;

@WebServlet("/ViewDetails")
public class ViewDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONObject jsonObject = null;
		try {
			jsonObject = new JSONObject(EmployeeManagement.viewRecord());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		response.getWriter().println(jsonObject);
	}

}
